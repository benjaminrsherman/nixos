{ lib, pkgs, ... }:

{
	home = {
		homeDirectory = "/home/benjamin";
		username = "benjamin";

	};

	programs = {
		alacritty = {
			enable = true;
			settings = {
				font.normal.family = "Ubuntu Mono";
			};
		};
		bat.enable = true;
		direnv = {
			enable = true;
			enableFishIntegration = true;
			enableNixDirenvIntegration = true;
		};
		feh.enable = true;
		firefox.enable = true;
		fish = import ./fish.nix;
		git = import ./git.nix;
		home-manager.enable = true;
		htop.enable = true;
		i3status = import ./i3status.nix;
		neomutt = {
			enable = true;
			vimKeys = true;
		};
		neovim = import ./neovim.nix pkgs;
		rtorrent.enable = true;
		ssh.enable = true;
		texlive.enable = true;
		zathura.enable = true;
	};

	services = {
		blueman-applet.enable = true;
		dunst = import ./dunst.nix;
		network-manager-applet.enable = true;
		unclutter.enable = true;
	};

	xdg = import ./xdg.nix;
	xsession = {
		enable = true;
		windowManager.i3 = import ./i3.nix lib;
	};
}

lib:

{
	enable = true;
	config = {
		modifier = "Mod4";
		bars = []; # no bars
		focus.mouseWarping = false;

		terminal = "alacritty";

		keybindings =
			let modifier = "Mod4"; in lib.mkOptionDefault {
			"XF86AudioRaiseVolume" = "\$HOME/.local/bin/pavolume +5%";
			"XF86AudioLowerVolume" = "\$HOME/.local/bin/pavolume -5%";
			"XF86AudioMute" = "\$HOME/.local/bin/pavolume toggle";

			"XF86MonBrightnessUp" = "xbacklight -inc 5";
			"XF86MonBrightnessDown" = "xbacklight -dec 5";

			"${modifier}+Shift+x" = "i3lock -c 0000000 -f";

			"${modifier}+Shift+b" = "maim -s -u | xclip -selection clipboard -t image/png";
			"${modifier}+b" = "maim -u | xclip -selection clipboard -t image/png";

			"${modifier}+Shift+o" = "reboot";
			"${modifier}+Shift+p" = "poweroff";

			"${modifier}+Shift+comma" = "dnd";
		};

		window = {
			border = 1;
			hideEdgeBorders = "both";
			titlebar = false;
		};
	};
}

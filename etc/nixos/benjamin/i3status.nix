{
	enable = true;
	enableDefault = false;
	general = {
		colors = true;
		interval = 1;
	};

	modules = {
		"path_exists DND" = {
			position = 1;
			settings = {
				path = "/tmp/dnd";
				format = "DND";
				format_down = "";
				color_good = "#FF0000";
			};
		};

		"volume master" = {
			position = 2;
			settings = {
				format = "♪: %volume";
				formal_muted = "♪: muted (%volume)";
				device = "default";
				mixer = "Master";
				mixer_idx = 0;
			};
		};

		"wireless _first_" = {
			position = 3;
			settings = {
				format_up = "W: %bitrate at %essid";
				format_down = "W: down";
			};
		};

		"battery all" = {
			position = 4;
			settings = {
				format = "%status %percentage %remaining";
				last_full_capacity = true;
				hide_seconds = true;
				integer_battery_capacity = true;
			};
		};

		"tztime local" = {
			position = 5;
			settings = {
				format = "%Y-%m-%d %H:%M";
			};
		};
	};
}

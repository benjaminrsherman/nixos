{
	enable = true;
	functions = {
		cd = "builtin cd $argv; ls";
		fish_prompt = "
			set_color $fish_color_cwd
			echo -n -s (hostname)': '(basename (prompt_pwd))' $ '
			";
		fish_right_prompt = "
			set_color $fish_color_cwd
			echo -n -s (prompt_pwd)
			";
	};
}

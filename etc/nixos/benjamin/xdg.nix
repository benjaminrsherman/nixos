{
	enable = true;
	userDirs = {
		enable = true;
		desktop = "\$HOME/desktop";
		documents = "\$HOME/documents";
		download = "\$HOME/downloads";
		music = "\$HOME/media/music";
		pictures = "\$HOME/media/pictures";
		videos = "\$HOME/media/videos";
	};
}

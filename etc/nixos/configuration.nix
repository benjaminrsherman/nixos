# Edit this configuration file to define what should be installed on
# your system.	Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
	imports =
		[ # Include the results of the hardware scan.
			./hardware-configuration.nix
			<home-manager/nixos>
		];

	nix = {
		autoOptimiseStore = true;
		maxJobs = "auto";
	};

	# Use the systemd-boot EFI boot loader.
	boot.loader.systemd-boot = {
		enable = true;
		editor = false;
	};
	boot.loader.efi.canTouchEfiVariables = true;

	networking = {
		hostName = "valkyrie-mobile";
		networkmanager.enable = true;
	};


	i18n.defaultLocale = "en_US.UTF-8";
	console = {
		font = "Lat2-Terminus16";
		keyMap = "us";
	};

	time.timeZone = "America/New_York";

	documentation = {
		# I want all documentation
		enable = true;
		dev.enable = true;
		doc.enable = true;
		info.enable = true;
		man.enable = true;
		nixos.enable = true;
	};

	environment.shells = [ pkgs.fish ];
	nixpkgs.config.allowUnfree = true;
	environment.systemPackages = with pkgs; [
		# utilities
		file
		git
		htop
		ripgrep
		rtorrent
		unzip
		which
		whois
		wget

		# programming
		cargo
		python
		python3
		clang

		# editors
		vim
		neovim

		# graphical
		discord
		firefox
		gimp
		steam
		vlc

		# misc
		aspell
		man-pages
	];

	hardware = {
		acpilight.enable = true;

		bluetooth.enable = true;

		cpu.amd.updateMicrocode = true;
		cpu.intel.updateMicrocode = true;

		sane.enable = true; # For DS
	};

	# List services that you want to enable:

	# Enable CUPS to print documents.
	services.printing.enable = true;

	# Enable sound.
	sound = {
		enable = true;
		mediaKeys = {
			enable = true;
			volumeStep = "5%";
		};
	};
	hardware.pulseaudio.enable = true;

	# Enable the X11 windowing system.
	services.xserver.enable = true;
	services.xserver.layout = "us";
	services.xserver.xkbOptions = "eurosign:e";

	# Enable touchpad support.
	services.xserver.libinput.enable = true;

	users = {
		defaultUserShell = pkgs.fish;
		mutableUsers = false;

		users.benjamin = {
			isNormalUser = true;
			extraGroups = [ "wheel" ];
			passwordFile = "/etc/secrets/benjamin_user_pass";
		};
	};
	home-manager.users.benjamin = import ./benjamin/configuration.nix;

	# This value determines the NixOS release from which the default
	# settings for stateful data, like file locations and database versions
	# on your system were taken. It‘s perfectly fine and recommended to leave
	# this value at the release version of the first install of this system.
	# Before changing this value read the documentation for this option
	# (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
	system.stateVersion = "20.03"; # Did you read the comment?

}

